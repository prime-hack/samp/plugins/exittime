import core.sys.windows.windows, core.sys.windows.dll;
import std.concurrency : spawn, yield;
import std.file : exists, isFile;
import std.path : dirName;
import core.runtime;

import llmo, inifiled;

__gshared HINSTANCE g_hinst;

extern (Windows) BOOL DllMain(HINSTANCE hInstance, ULONG ulReason, LPVOID) {
	g_hinst = hInstance;
	final switch (ulReason) {
	case DLL_PROCESS_ATTACH:
		Runtime.initialize();
		spawn({
			while (*cast(uint*)0xC8D4C0 < 9)
				yield();
			init();
		});
		dll_process_attach(hInstance, true);
		break;
	case DLL_PROCESS_DETACH:
		Runtime.terminate();
		dll_process_detach(hInstance, true);
		break;
	case DLL_THREAD_ATTACH:
		dll_thread_attach(true, true);
		break;
	case DLL_THREAD_DETACH:
		dll_thread_detach(true, true);
		break;
	}
	return true;
}

/// Initialize plugin: set hacks
void init() {
	// Load settings
	char[260] path;
	GetModuleFileNameA(g_hinst, path.ptr, 260);
	auto cfgFile = cast(string)path.dirName ~ "/exitTime.ini";
	if (cfgFile.exists && cfgFile.isFile)
		cfg.readINIFile(cfgFile);
	else
		cfg.writeINIFile(cfgFile);

	immutable samp = cast(size_t)GetModuleHandleA("samp");

	if (samp && samp != cast(size_t)INVALID_HANDLE_VALUE) {
		if (cfg.time >= 0)
			writeMemory(cast(int*)(samp + 0x9ED79), cfg.time, MemorySafe.prot); // set time
		else
			writeMemory(cast(int*)(samp + 0x9ED79), 0, MemorySafe.prot); // set time
		simpleCallHook(samp + 0xB28DE, cast(void*)&hook_GetTickCount, 6); // set hook on calling GetTickCount in CloseGame
	}
}

extern (Windows) DWORD hook_GetTickCount() {
	if (cfg.time == 0)
		writeMemory(cast(void*)&Sleep, [0xC2, 0x04, 0x00], MemorySafe.prot);
	else if (cfg.time == -1)
		ExitProcess(0);
	else if (cfg.time == -2)
		TerminateProcess(GetCurrentProcess(), 0);
	return GetTickCount(); // original code
}

@INI("Settings", "settings")
struct Config {
	@INI("Time in milliseconds") int time = 1000;
}

__gshared Config cfg;
